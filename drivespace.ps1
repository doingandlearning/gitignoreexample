# Get the available disk space on the C drive
$drive = "C:"
$freeSpace = Get-PSDrive -PSProvider FileSystem | Where-Object {$_.Root -eq $drive} | Select-Object -ExpandProperty Free

# Convert the free space to GB and output the result
$freeSpaceGB = [Math]::Round($freeSpace / 1GB, 2)
Write-Output "Free space on drive $drive: $freeSpaceGB GB"