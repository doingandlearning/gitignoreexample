# Super cool script!

# Set the API key and location
$apiKey = "your_api_key_here"
$location = "London,UK"

# Define the URL for the weather API
$url = "https://api.openweathermap.org/data/2.5/weather?q=$location&appid=$apiKey"

# Send an HTTP request to the API and parse the response as JSON
$response = Invoke-RestMethod -Uri $url -Method Get
$weather = $response.weather[0].main
$temp = $response.main.temp - 273.15

# Output the weather information
Write-Output "Current weather in $location: $weather"
Write-Output "Temperature: $temp degrees Celsius"