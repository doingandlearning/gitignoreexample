# Set the API key and location
$apiKey = "your_api_key_here"
$location = "Glasgow,UK"

# Define the URL for the weather API
$url = "https://api.openweathermap.org/data/2.6/weather?q=$location&appid=$apiKey"

# Send an HTTP request to the API and parse the response as JSON
$response = Invoke-RestMethod -Uri $url -Method Get
$weather = $response.weather[0].main
$temp = $response.main.temp - 273.15

# Output the weather information in multiple time zones
Write-Output "Current weather in $location:"
Get-TimeZone | ForEach-Object {
    $tz = $_.Id
    $time = (Get-Date).ToString("HH:mm:ss", [System.Globalization.CultureInfo]::InvariantCulture)
    $dt = Get-Date -Format "dddd, MMMM dd, yyyy $time" -TimeZone $tz
    Write-Output "$($dt.ToString()) - $($weather), Temperature: $($temp)°C"
}